﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4D.Api.Models
{
    public class SupplierViewModel
    {
        public int Id { get; set; } 
        public int OwnerType { get; set; } 
        public string Name { get; set; } 
        public int CountryId { get; set; } 
        public int Ico { get; set; } 
        public int Vat { get; set; } 
        public int CurrencyCode { get; set; }  
        public string Street { get; set; } 
        public string City { get; set; }  
        public string PostCode { get; set; }  
        public int Type { get; set; }  
        public bool Main { get; set; } 
    }
}
