﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAngularJsUploader.Models
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public long Size { get; set; }
        public string Message { get; set; }
        public string Receiver { get; set; }
        public string Subject { get; set; }
        public int Id { get; set; }

    }
}