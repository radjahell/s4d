﻿using S4D.Api.FirstModels;
using S4D.Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace S4D.Api.Controllers
{

    public class PriceListController : ApiController
    {
        private s4dTest1Context context = new s4dTest1Context();

        [Route("api/getpricelisttypes")]
        [HttpGet]
        public IQueryable<FirstModels.TypePriceList> GetPriceListTypes()
        {
            return context.TypePriceList.Select(x => x);

        }

        [Route("api/getdatatypes")]
        [HttpGet]
        public IQueryable<FirstModels.TypeData> GetDataTypes()
        {
            return context.TypeData.Select(x => x);
        }

        [Route("api/getbuyers")]
        [HttpGet]
        public IQueryable<FirstModels.Buyer> GetBuyers()
        {
            return context.Buyer.Select(x => x);
        }

        [Route("api/savepriceList")]
        [HttpPost]
        public void SavePriceList([FromBody]FirstModels.PriceList priceList)
        {
            context.PriceList.Add(priceList);
            context.SaveChanges();
        }

        [Route("api/saveFileInfo/{senderId}/{buyerId}/{fileId}")]
        [HttpPost]
        public void PostFileInfo(int senderId, int buyerId, int fileId)
        { 
            var sentFileFromDb = context.SentFile.Where(x => x.Id == fileId).FirstOrDefault();
            sentFileFromDb.Message = "MyTest";

            context.SentFile.Attach(sentFileFromDb);
            context.Entry(sentFileFromDb).State = EntityState.Modified;
            context.SaveChanges(); 
        }

        //GET api/pricelist


        //// GET api/pricelist/5
        //public PriceListViewModel Get(int id)
        //{
        //   // var priceList = unitOfWork.PriceListRepository.GetById(1);

        //    var priceListModel = new PriceListViewModel();
        //    priceListModel.Id = 1;
        //    priceListModel.Description = "January PL"; //priceList.Description;
        //    priceListModel.BuyerName = "Jan Penkala"; //priceList.Buyer.FirstName + " " + priceList.Buyer.Surname;
        //    priceListModel.SellerName = "Xavier Montmartre"; //priceList.Seller.Firstname + " " + priceList.Seller.Surname; 
        //    priceListModel.Status = "Draft"; //priceList.TypePriceList.Status;
        //    priceListModel.StartDate = DateTime.Now; //priceList.StartDate;
        //    priceListModel.EndDate = DateTime.Now; //priceList.EndDate;
        //    return priceListModel;
        //}

        // POST api/values
        public void Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            throw new NotImplementedException();
            //unitOfWork.PriceListRepository.Delete(id);
        }
    }
}
