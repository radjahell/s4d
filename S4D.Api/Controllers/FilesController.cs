﻿using S4D.Api.FirstModels;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using Tanka.FileSystem.WebApi.FileSystem;
using Tanka.FileSystem.WebApi.FlowJS;

namespace S4D.Api.Controllers
{

    [RoutePrefix("folders")]
    public class FilesController : ApiController
    {
        private readonly Flow _flow;
        private readonly FileSystem _fileSystem;
        private s4dTest1Context priceListContext = new s4dTest1Context();

        public FilesController()
        {
            _fileSystem = new FileSystem
            {
                GetFilePathFunc = filePath => string.Format(
                    "{0}/{1}",
                    HostingEnvironment.MapPath("~/App_Data").Replace("\\", "/"),
                    filePath)
            };

            _flow = new Flow(_fileSystem);
        }

        [Route("{*filePath}")]
        public async Task<HttpResponseMessage> GetFile(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid path");
            }

            if (!await _fileSystem.ExistsAsync(filePath))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "File not found");
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StreamContent(await _fileSystem.OpenReadAsync(filePath));
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = filePath.Substring(filePath.LastIndexOf('/') + 1)
            };

            return response;
        }

        [Route("uploads/{folderName}")]
        public async Task<HttpResponseMessage> Get(string folderName)
        {
            //return new HttpResponseMessage(HttpStatusCode.BadRequest);
            var context = CreateContext(folderName);

            return await _flow.HandleRequest(context);
        }

        [Route("uploads/{folderName}")]
        public async Task<HttpResponseMessage> Post(string folderName)
        {
            var context = CreateContext(folderName);
            context.ProcessFileFunc = (flowRequest, flowContext) =>
            {
                var fileName = flowContext.GetFileName(flowRequest);
                var filePath = GetFilePath(flowContext.GetFilePath(flowRequest));

                filePath = Path.Combine(filePath, fileName);
                var data = System.IO.File.ReadAllBytes(filePath);

                //var fileInfo = new FileInfo(file.LocalFileName);

                SentFile sentFile = new SentFile();
                sentFile.Created = DateTime.Now;
                sentFile.FileData = data;
                sentFile.FileName = fileName;

                //SAVE FILE TO DB
                priceListContext.SentFile.Add(sentFile);
                priceListContext.SaveChanges();

                //GET ID OF SAVED FILE
                int id = sentFile.Id; 

                //DELETE TEMPORARY FILE
                File.Delete(filePath);

                var response = flowContext.HttpRequest.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(id.ToString());

                return response;
            };


            return await _flow.HandleRequest(context);
        }

        [Route("uploads/getFile/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetFile(int id)
        {
            var firstOrDefault = priceListContext.SentFile.FirstOrDefault(x => x.Id == id);
            if (firstOrDefault != null)
            {
                var fileBytes = firstOrDefault.FileData;
                var fileName = firstOrDefault.FileName;

                var filePath = HostingEnvironment.MapPath("~/App_Data").Replace("\\", "/");
                File.WriteAllBytes(filePath + "/" + fileName, fileBytes);


            }


            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }


        private FlowRequestContext CreateContext(string folderName)
        {
            return new FlowRequestContext(Request)
            {
                GetChunkFileNameFunc = parameters => string.Format(
                    "{1}_{0}.chunk",
                    parameters.FlowIdentifier,
                    parameters.FlowChunkNumber.Value.ToString().PadLeft(8, '0')),
                GetChunkPathFunc = parameters => string.Format("{0}/chunks/{1}", folderName, parameters.FlowIdentifier),
                GetFileNameFunc = parameters => parameters.FlowFilename,
                GetFilePathFunc = parameters => folderName,
                GetTempFileNameFunc = filePath => string.Format("file_{0}.tmp", Guid.NewGuid()),
                GetTempPathFunc = () => string.Format("{0}/temp", folderName),
                MaxFileSize = ulong.MaxValue
            };
        }

        private static string GetFilePath(string filePath)
        {
            var path = HostingEnvironment.MapPath("~/App_Data");
            return string.IsNullOrEmpty(path) ? filePath : string.Format("{0}/{1}", path.Replace("\\", "/"), filePath);
        }

    }
}