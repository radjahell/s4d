﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class TypeAddress
    { 
        public TypeAddress()
        {
            Addresses = new HashSet<Address>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int TypeAddressId { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
 
        public virtual ICollection<Address> Addresses { get; set; }
    }
}