﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class SentFile
    {
        public SentFile()
        {
        }

        public int Id { get; set; }

        public byte[] FileData { get; set; }

        [StringLength(100)]
        public string FileName { get; set; }

        public DateTime? Created { get; set; } 

        public int? Size { get; set; }

        [StringLength(400)]
        public string Message { get; set; } 

    }
}