﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4D.Api.FirstModels.ViewModels
{
    public class PriceListViewModel
    {
        public int SellerId { get; set; }
        public int BuyerId { get; set; }
        public int TypePriceListId { get; set; }
        public int TypeDataId { get; set; }
        public string ListName { get; set; }

        public string Description { get; set; }

        public string Body { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime Created { get; set; } 

    }
}
