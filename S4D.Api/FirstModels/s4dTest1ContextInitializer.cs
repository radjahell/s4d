﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class s4dTest1ContextInitializer : DropCreateDatabaseIfModelChanges<s4dTest1Context>
    {
        protected override void Seed(s4dTest1Context context)
        {

            //Seed Type Address

            var typeAddressOne = new TypeAddress() { Type = "Corespondent" , Description="xx" };
            context.TypeAddress.Add(typeAddressOne);

            //Seed Buyer 
            var buyerOne = new Buyer() { Email = "email@xx.com", FirstName = "JAN", Surname = "Penkala", Description = "XX", BuyerId = 1 };
            var buyer2 = new Buyer() { Email = "jossif@xx.com", FirstName = "Jossif", Surname = "Mifsud", Description = "XX", BuyerId = 2 };
            var buyer3 = new Buyer() { Email = "kubus@xx.com", FirstName = "Michal", Surname = "Kubus", Description = "XX", BuyerId = 3 };

            IList<Buyer> buyerInit = new List<Buyer>();
            buyerInit.Add(buyerOne);
            buyerInit.Add(buyer2);
            buyerInit.Add(buyer3);
            foreach (Buyer receiver in buyerInit)
                context.Buyer.Add(receiver);


            //Seed Seller 
            var sellerOne = new Seller() { FirstName = "Petr", Surname = "Kubica", Description = "XX", SellerId = 1 };
            IList<Seller> sellerInit = new List<Seller>();
            sellerInit.Add(sellerOne);
            foreach (Seller sender in sellerInit)
                context.Seller.Add(sender);

            //Seed Adddress
            var addresOne = new Address() { City = "Moscow", PostCode = "72525", Street = "vYCHODNI", Buyer = buyerOne, TypeAddress = typeAddressOne};
            context.Address.Add(addresOne);

            //Seed dataType
            var typeDataOne = new TypeData() { Type = "Listing Product" };
            IList<TypeData> typeDataInitilizer = new List<TypeData>();
            typeDataInitilizer.Add(typeDataOne);
            typeDataInitilizer.Add(new TypeData() { Type = "Listing Supplier" });
            typeDataInitilizer.Add(new TypeData() { Type = "Price List" }); 
            foreach (TypeData typeData in typeDataInitilizer)
                context.TypeData.Add(typeData);


            //Seed priceListTypes 
            var typePriceListOne = new TypePriceList() { Type = "Promo" };
            IList<TypePriceList> priceListTypeInitializer = new List<TypePriceList>();
            priceListTypeInitializer.Add(typePriceListOne);
            priceListTypeInitializer.Add(new TypePriceList() { Type = "Listing" });
            priceListTypeInitializer.Add(new TypePriceList() { Type = "Standart" });
            priceListTypeInitializer.Add(new TypePriceList() { Type = "Special" });
            foreach (TypePriceList priceListType in priceListTypeInitializer)
                context.TypePriceList.Add(priceListType);


            var priceListInitializer = new PriceList()
            {
                Body = "My",
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Description = "MyPRiceList",
                Buyer = buyerOne,
                Seller = sellerOne,
                TypeData = typeDataOne,
                TypePriceList = typePriceListOne

            };
            context.PriceList.Add(priceListInitializer);



            ////Seed Initializer
            //IList<Receiver> receiverInitializer = new List<Receiver>();
            //receiverInitializer.Add(new Receiver() { FirstName = "Jan", Surname = "Penkala", Email = "penkis@yopmail.com" });
            //receiverInitializer.Add(new Receiver() { FirstName = "Jiri", Surname = "Neuvirth", Email = "nojvy@yopmail.com" });
            //receiverInitializer.Add(new Receiver() { FirstName = "Petr", Surname = "Kubica", Email = "kuby@yopmail.com" });
            //foreach (Receiver receiver in receiverInitializer)
            //    context.Receiver.Add(receiver);

            //IList<PriceList> priceListInitializer = new List<PriceList>();
            //priceListInitializer.Add(new PriceList() { Created = new DateTime(), EndDate = new DateTime(), StartDate = new DateTime(), Body = "XX" });

            //foreach (Receiver priceList in priceListInitializer)
            //    context.PriceList.Add(priceList);


            base.Seed(context);
        }
    }
}