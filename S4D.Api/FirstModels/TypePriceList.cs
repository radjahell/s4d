﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class TypePriceList
    {
        public TypePriceList()
        {
            PriceLists = new HashSet<PriceList>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int TypePriceListId { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public virtual ICollection<PriceList> PriceLists { get; set; }
    }
}