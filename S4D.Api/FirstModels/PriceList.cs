﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class PriceList
    {
        public PriceList()
        {

        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Column(TypeName = "xml")]
        public string Body { get; set; }

        [StringLength(200)]
        public string Description { get; set; }


        public int BuyerId { get; set; }
        //[ForeignKey("BuyerId")]
        public virtual Buyer Buyer { get; set; }


        public int SellerId { get; set; }
        //[ForeignKey("SellerId")]
        public virtual Seller Seller { get; set; }


        public int TypePriceListId { get; set; }
        //[ForeignKey("TypePriceListId")]
        public virtual TypePriceList TypePriceList { get; set; }


        public int TypeDataId { get; set; }
        //[ForeignKey("TypeDataId")]
        public virtual TypeData TypeData { get; set; }

        public byte[] File { get; set; }
    }
}