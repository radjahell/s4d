﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class Buyer
    {
        public Buyer()
        {
            PriceLists = new HashSet<PriceList>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int BuyerId { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string Surname { get; set; }

        [Required]
        public string Email { get; set; }

        [StringLength(200)]
        public string Description { get; set; } 

        public virtual ICollection<PriceList> PriceLists { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}