﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class s4dTest1Context : DbContext
    {

        public s4dTest1Context() : base ("name=FilesContext")
        {
            Database.SetInitializer(new s4dTest1ContextInitializer());
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //FOR FUTURE MAPPING

            modelBuilder.Entity<TypeAddress>()
       .HasMany(e => e.Addresses)
       .WithRequired(e => e.TypeAddress)
       .WillCascadeOnDelete(false);

            modelBuilder.Entity<Buyer>()
           .HasMany(e => e.Addresses)
           .WithRequired(e => e.Buyer)
           .WillCascadeOnDelete(false);

            modelBuilder.Entity<Seller>()
             .HasMany(e => e.PriceLists)
             .WithRequired(e => e.Seller)
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<Buyer>()
                .HasMany(e => e.PriceLists)
                .WithRequired(e => e.Buyer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypePriceList>()
                .HasMany(e => e.PriceLists)
                .WithRequired(e => e.TypePriceList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeData>()
               .HasMany(e => e.PriceLists)
               .WithRequired(e => e.TypeData)
               .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TypeAddress>()
            //    .HasMany(e => e.Addresses)
            //    .WithRequired(e => e.TypeAddress)
            //    .HasForeignKey(e => e.Type)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TypeCountry>()
            //    .Property(e => e.Code)
            //    .IsUnicode(false);

            //modelBuilder.Entity<TypeCountry>()
            //    .HasMany(e => e.Suppliers)
            //    .WithRequired(e => e.TypeCountry)
            //    .HasForeignKey(e => e.CountryId)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TypeOwner>()
            //    .HasMany(e => e.Suppliers)
            //    .WithRequired(e => e.TypeOwner)
            //    .HasForeignKey(e => e.OwnerType)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TypePriceList>()
            //    .HasMany(e => e.PriceLists)
            //    .WithRequired(e => e.TypePriceList)
            //    .HasForeignKey(e => e.TypeId)
            //    .WillCascadeOnDelete(false);
        }

        public DbSet<PriceList> PriceList { get; set; }
        public DbSet<TypePriceList> TypePriceList { get; set; }
        public DbSet<SentFile> SentFile { get; set; }
        public DbSet<TypeData> TypeData { get; set; } 
        public DbSet<Buyer> Buyer { get; set; }
        public DbSet<Seller> Seller { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<TypeAddress> TypeAddress { get; set; }





    }
}