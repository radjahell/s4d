﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace S4D.Api.FirstModels
{
    public class Address
    {
        public Address()
        {

        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int AddressId { get; set; } 
        
        [StringLength(100)]
        public string Street { get; set; } 
        
        [StringLength(50)]
        public string City { get; set; }  
         
        [StringLength(50)]
        public string PostCode { get; set; }   

        public int BuyerId  { get; set; }
        //[ForeignKey("BuyerId")]
        public virtual Buyer Buyer { get; set; }

        public int TypeAddressId { get; set; }
        //[ForeignKey("TypeAddressId")]
        public virtual TypeAddress TypeAddress  { get; set; }


    }
}