﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4D.Mailer
{
    public class Example
    {
        private static void Main()
        {
            Execute().Wait();
        }

        public static async Task Execute()
        {
            string apiKey = Environment.GetEnvironmentVariable("SENDGRID_KEY", EnvironmentVariableTarget.User);
            dynamic sg = new SendGridAPIClient(apiKey);

            Email from = new Email("test@example.com");
            string subject = "Hello World from the SendGrid CSharp Library!";
            Email to = new Email("s4dTest@yopmail.com");
            Content content = new Content("text/plain", "Hello, Email!");
            Mail mail = new Mail(from, subject, to, content);

            try
            {
                dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
                Console.WriteLine("RMO: " + response);
            }
            catch (Exception ex)
            {

                throw ex;
            }

             
         
        }
    }
}
