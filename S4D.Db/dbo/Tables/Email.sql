﻿CREATE TABLE [dbo].[Email] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [OwnerType]   INT            NOT NULL,
    [OwnerId]     INT            NOT NULL,
    [Email]       NVARCHAR (100) NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED ([Id] ASC)
);

