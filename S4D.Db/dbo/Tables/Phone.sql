﻿CREATE TABLE [dbo].[Phone] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [OwnerType]    INT            NOT NULL,
    [OwnerId]      INT            NOT NULL,
    [PhoneNumber]  INT            NOT NULL,
    [FaxNumber]    INT            NOT NULL,
    [MobileNumber] INT            NOT NULL,
    [Description]  NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Phone] PRIMARY KEY CLUSTERED ([Id] ASC)
);

