﻿CREATE TABLE [dbo].[Buyer] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [OwnerType]   INT            NOT NULL,
    [FirstName]   NVARCHAR (100) NOT NULL,
    [Surname]     NVARCHAR (100) NOT NULL,
    [CountryId]   INT            NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Buyer] PRIMARY KEY CLUSTERED ([Id] ASC)
);

