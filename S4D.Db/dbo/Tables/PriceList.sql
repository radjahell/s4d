﻿CREATE TABLE [dbo].[PriceList] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [SellerId]    INT            NOT NULL,
    [BuyerId]     INT            NOT NULL,
    [Created]     DATETIME       NOT NULL,
    [StartDate]   DATETIME       NOT NULL,
    [EndDate]     DATETIME       NOT NULL,
    [Body]        XML            NOT NULL,
    [StatusId]    INT            NOT NULL,
    [TypeId]      INT            NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_PriceList] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PriceList_Buyer] FOREIGN KEY ([BuyerId]) REFERENCES [dbo].[Buyer] ([Id]),
    CONSTRAINT [FK_PriceList_Seller] FOREIGN KEY ([SellerId]) REFERENCES [dbo].[Seller] ([Id]),
    CONSTRAINT [FK_PriceList_TypePriceList] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[TypePriceList] ([Id])
);

