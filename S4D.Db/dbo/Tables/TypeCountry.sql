﻿CREATE TABLE [dbo].[TypeCountry] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (2)    NOT NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_TypeCountry] PRIMARY KEY CLUSTERED ([Id] ASC)
);

