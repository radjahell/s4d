﻿CREATE TABLE [dbo].[TypePriceListStatus] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Status]      NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_TypePriceListStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

