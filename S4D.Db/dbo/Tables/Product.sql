﻿CREATE TABLE [dbo].[Product] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [Type]                NVARCHAR (100) NOT NULL,
    [Ean]                 NVARCHAR (100) NOT NULL,
    [InternalProductCode] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC)
);

