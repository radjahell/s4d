﻿CREATE TABLE [dbo].[TypeOwner] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Type]        NVARCHAR (2)   NOT NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_TypeOwner] PRIMARY KEY CLUSTERED ([Id] ASC)
);

