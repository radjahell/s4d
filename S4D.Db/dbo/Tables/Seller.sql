﻿CREATE TABLE [dbo].[Seller] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [SupplierId]  INT            NOT NULL,
    [Firstname]   NVARCHAR (100) NOT NULL,
    [Surname]     NVARCHAR (100) NOT NULL,
    [CountryId]   INT            NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Seller] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Seller_Supplier] FOREIGN KEY ([SupplierId]) REFERENCES [dbo].[Supplier] ([Id])
);

