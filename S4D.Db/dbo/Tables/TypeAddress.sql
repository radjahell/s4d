﻿CREATE TABLE [dbo].[TypeAddress] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Type]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_TypeAddress] PRIMARY KEY CLUSTERED ([Id] ASC)
);

