﻿CREATE TABLE [dbo].[ReceivedFiles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FileData] [varbinary](max) NULL,
	[FileName] [nvarchar](100) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[Size] [int] NULL,
	[Message] [nvarchar](400) NULL,
	[Subject] [nvarchar](100) NULL,
	[Receiver] [nvarchar](100) NULL,
 CONSTRAINT [PK_ReceivedFiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]