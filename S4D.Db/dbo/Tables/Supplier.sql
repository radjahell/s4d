﻿CREATE TABLE [dbo].[Supplier] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [OwnerType]    INT            NOT NULL,
    [Name]         NVARCHAR (100) NOT NULL,
    [CountryId]    INT            NOT NULL,
    [Ico]          INT            NOT NULL,
    [Vat]          INT            NOT NULL,
    [CurrencyCode] VARCHAR (3)    NOT NULL,
    CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Supplier_TypeCountry] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[TypeCountry] ([Id]),
    CONSTRAINT [FK_Supplier_TypeOwner] FOREIGN KEY ([OwnerType]) REFERENCES [dbo].[TypeOwner] ([Id])
);

