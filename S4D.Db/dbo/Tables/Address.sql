﻿CREATE TABLE [dbo].[Address] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [OwnerType] INT            NOT NULL,
    [OwnerId]   INT            NOT NULL,
    [Street]    NVARCHAR (100) NOT NULL,
    [City]      NVARCHAR (50)  NOT NULL,
    [PostCode]  NVARCHAR (50)  NOT NULL,
    [Type]      INT            NOT NULL,
    [Main]      BIT            NOT NULL,
    CONSTRAINT [FK_Address_TypeAddress] FOREIGN KEY ([Type]) REFERENCES [dbo].[TypeAddress] ([Id])
);

