﻿

angular.module('jarapp')
.controller('FilesController', ['filesManagerRepository', 'ngTableParams','alertService', function (filesManagerRepository, ngTableParams, alertService) {

    var vm = this;
    vm.title = 'file manager';
    vm.files = [];

    filesManagerRepository.getFiles()
           .then(function (files) {

               vm.files = files.data;

           }).catch(function(response) {
               alertService.add('error', 'Error while retreving files. Error: ' + response.statusText);
        }); 
    }]);