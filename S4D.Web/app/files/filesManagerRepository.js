﻿ 

angular.module('jarapp')
    .service('filesManagerRepository', [
        '$http', 'appSettings', function ($http, appSettings) {

            this.getFiles = function () {
                var files = $http.get(appSettings.serverPath + '/api/file');
                return files;
            }; 
          
            this.sendFile = function (formData) {
                 return $http.post(appSettings.serverPath + '/api/file', formData, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    });
            };

            this.deleteFile = function (id) {

            };

         }]);
    