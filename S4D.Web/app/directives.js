﻿(function () {
    'use strict';

    angular.module('boDirectives', ['ui.bootstrap.datetimepicker', 'boDataType'])
    .directive('ngConfirmClick', [
        // Directive that shows a confirmation popup with the given text (directive's attribute value).
        // Popup's user actions:
        // - "Ok" button click: The action of define in the "ng-click" directive and the popup is closed.
        // - "Cancel" button click: The popup is closed.
        // Source: http://zachsnow.com/#!/blog/2013/confirming-ng-click/
      function () {
          return {
              priority: -1,
              restrict: 'A',
              link: function (scope, element, attrs) {
                  element.bind('click', function (e) {
                      var message = attrs.ngConfirmClick;
                      if (message && !confirm(message)) {
                          e.stopImmediatePropagation();
                          e.preventDefault();
                      }
                  });
              }
          };
      }
    ])
    .directive('boDateInput', ['$filter', function ($filter) {
        /// <summary>
        /// Directive that makes an input[text] displaying and editing dates using the BO date/time formats.
        /// It could be configured to manage either a date or a datetime.
        /// </summary>
        /// Examples:
        /// <input type="text" bo-date-input="datetime" ng-model="myDateTime"/>

        var dateMode = { code: 'date', parseFormat: 'DD/MM/YYYY', displayFilterName: 'boDate' };
        var dateTimeMode = { code: 'datetime', parseFormat: 'DD/MM/YYYY HH:mm:ss', displayFilterName: 'boDateTime' };

        return {
            require: 'ngModel',
            restrict: 'A',
            priority: 15,
            controller: function () { },
            link: function (scope, element, attrs, ngModelController) {

                // Select the current mode
                var definedModeCode = attrs.boDateInput.toLowerCase();
                var currentMode;
                if (!attrs.boDateInput || definedModeCode == dateMode.code) {
                    currentMode = dateMode;
                }
                else if (definedModeCode == dateTimeMode.code) {
                    currentMode = dateTimeMode;
                }
                else {
                    throw Error("The 'boDateInput' directive must be configured in one of the following modes: 'date', 'datetime'");
                }

                ngModelController.$parsers.unshift(function (data) {
                    //convert data from view format to model format
                    var momentDate = moment(data, currentMode.parseFormat, true);

                    if (!momentDate || !momentDate.isValid()) {
                        setModelValidity(false);
                        return null;
                    }

                    // Set validity OK, and return data
                    setModelValidity('format', true);
                    return momentDate.toDate();
                });

                ngModelController.$formatters.unshift(function (data) {
                    if (!data) return null;

                    //convert data from model format to view format
                    var momentDate = moment(data);

                    if (!momentDate.isValid()) return null;

                    setModelValidity(true);
                    return $filter(currentMode.displayFilterName)(momentDate.toDate());
                });

                var setModelValidity = function (isValid) {
                    ngModelController.$setValidity('format', isValid);
                };
            }
        };
    }])
    .directive('min', [function () {
        /// <summary>
        /// Directive that defines a minimum allowed date for a boDateInput.
        /// </summary>
        return {
            restrict: 'A',
            require: ['?ngModel', '?boDateInput'],
            priority: 9, // lower priority than boDateInput
            link: function (scope, element, attrs, controllers) {
                if (!areRequiredControllersPresent(controllers)) return; // If required controllers aren't present, continue

                var ngModelCtrl = controllers[0]; // Get ngmodel controller (first one in the "require" property above)

                setupNgModelValidation('min', ngModelCtrl, function (viewValue) {
                    var momentViewValue = moment(viewValue);
                    var momentMinimumDate = moment(scope.$eval(attrs.min));
                    if (!momentViewValue.isValid() || !momentMinimumDate.isValid()) return true;
                    return momentViewValue >= momentMinimumDate;
                });
            }
        };
    }])
    .directive('max', [function () {
        /// <summary>
        /// Directive that defines a maximum allowed date for a boDateInput.
        /// </summary>
        return {
            restrict: 'A',
            require: ['?ngModel', '?boDateInput'],
            priority: 9, // lower priority than boDateInput
            link: function (scope, element, attrs, controllers) {
                if (!areRequiredControllersPresent(controllers)) return; // If required controllers aren't present, continue

                var ngModelCtrl = controllers[0]; // Get ngmodel controller (first one in the "require" property above)

                setupNgModelValidation('max', ngModelCtrl, function (viewValue) {
                    var momentViewValue = moment(viewValue);
                    var momentMaximumDate = moment(scope.$eval(attrs.max));
                    if (!momentViewValue.isValid() || !momentMaximumDate.isValid()) return true;
                    return momentViewValue <= momentMaximumDate;
                });
            }
        }
    }])
    .directive('ngOnlyDigits', [function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) return;
                ngModel.$parsers.unshift(function (inputValue) {
                    var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                    ngModel.$viewValue = digits;
                    ngModel.$render();
                    return digits;
                });
            }
        };
    }]);

    var areRequiredControllersPresent = function (controllers) {
        if (controllers.length != 2) return false;
        if (!controllers[0] || !controllers[1]) return false;
        return true;
    };

    var setupNgModelValidation = function (validatityPropertyName, ngModelController, validityEvaluationFunction) {
        /// <summary>
        /// Function that setup a custom validation on the given ngModel controller.
        /// </summary>
        /// <param name="validatityPropertyName">Name of the validation property (e.g. that will be available in $error property).</param>
        /// <param name="ngModelController">The ngModel controller.</param>
        /// <param name="validityEvaluationFunction">Function that evaluates the validity of the given value. Should return true if the given value is valid, false otherwise.</param>
        var validate = function (viewValue) {
            if (!viewValue || validityEvaluationFunction(viewValue)) {
                // it is valid
                ngModelController.$setValidity(validatityPropertyName, true);
                return viewValue;
            } else {
                // it is invalid, return undefined (no model update)
                ngModelController.$setValidity(validatityPropertyName, false);
                return viewValue;
            }
        };

        ngModelController.$parsers.push(validate);
        ngModelController.$formatters.push(validate);
    };
})();
