﻿

angular.module('jarapp')
        .controller('DownloadPriceListDetailsController', ['$rootScope', '$scope', '$state', 'translateService', 'priceListService', function ($rootScope, $scope, $state , translateService, priceListService) {

        var downloadPriceListDetails = this;
      
        downloadPriceListDetails.priceList = $rootScope.priceList;
        downloadPriceListDetails.gridOptions = $rootScope.priceList.gridOptions;

        downloadPriceListDetails.goToDataPort = function () {
                $state.go('home.dataPort'); 
            };


        downloadPriceListDetails.setStatusToApprove = function (id) {
            priceListService.setStatusToApprove(id);
        };

        downloadPriceListDetails.setStatusToDecline = function(id) {
            
        };

        downloadPriceListDetails.setStatusToFix = function(id) {
            
        };


        }]);