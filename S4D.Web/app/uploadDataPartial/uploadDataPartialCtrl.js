﻿

angular.module('jarapp')
        .controller('UploadDataPartialController', ['$window', '$q', '$rootScope', '$scope', 'ngTableParams', '$filter', 'repository', 'alertService', '$state', 'priceListService', '$sessionStorage', 'typesService', function ($window,$q, $rootScope, $scope, ngTableParams, $filter, repository, alertService, $state, priceListService, $sessionStorage, typesService) {

            $scope.loggedIn = $sessionStorage.loggedIn;
           




            var unbind = $rootScope.$on('NotifyPartialUploadDataPartialViewEvent', function () {
                $scope.loggedIn = $sessionStorage.loggedIn;
            });
            $scope.$on('$destroy', unbind);

            $scope.gridOptions = {};

            $scope.showPreview = true;
            $scope.excelFile = '';

            //function create_blob(file) {
            //    var deferred = $q.defer();
            //    var reader = new FileReader();
            //    reader.onload = function () {
            //        deferred.resolve(reader.result);
            //    };
            //    reader.readAsArrayBuffer(file);
            //    return deferred.promise;
            //}


            $scope.saveDataUploader = function (dataUploaderForm, dataUploader) {

                $scope.fileName = $scope.excelFile.name;
                $scope.fileUrl = $scope.excelFile.url;



                //var promise = create_blob($scope.excelFile._file);
                //promise.then(function(result) {


                var priceList = {
                    buyerId: 1,
                    sellerId: 1,
                    typePriceListId: 1,
                    typeDataId: 1,
                    //status: "Draft",
                    listName: dataUploader.listName,
                    //priceListType: "Pricelist",
                    description: $scope.excelFile.name,
                    //size: $scope.excelFile.size,
                    //url: $scope.excelFile.url,
                    body: $scope.excelFile.name,
                    startDate: dataUploader.startDate,
                    endDate: dataUploader.endDate,
                    created: dataUploader.startDate,
                    file: result
                };

                //debugger;

                repository.savePriceList(priceList)
           .then(function (response) {
               console.log(response);
           }).catch(function (error) {
               console.log(error);
           });

            };




                //SAVE PRICELIST
              


                //var reader = new FileReader();

                //reader.onload = function (evt) {
                //    $scope.$apply(function () {
                //        var data = evt.target.result;

                //        var workbook = XLSX.read(data, { type: 'binary' });
                //        var headerNames = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];
                //        var data = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);

                //        $scope.gridOptions.columnDefs = [];
                //        headerNames.forEach(function (h) {
                //            $scope.gridOptions.columnDefs.push({ field: h });
                //        });

                //        $scope.gridOptions.data = data;
                //    });
                //};

                //reader.readAsBinaryString($scope.excelFile._file);

                //priceListService.addPriceList(priceList, $scope.gridOptions);

              //  $state.go('home.dataPort');
            //};

            //GET PRICE LIST TYPES
            repository.getpricelisttypes()
                .then(function (response) {
                    $scope.priceListTypes = response.data;

                    $scope.selectedPriceListType = {
                        priceListType: $scope.priceListTypes[2]
                    };
                })
                  .catch(function (response) {
                      alertService.add('error', 'Error while getting pricelist types  Error: ' + response.statusText);
                  });

            //GET DATA TYPES
            repository.getdatatypes()
             .then(function (response) {
                 $scope.dataTypes = response.data;



                 $scope.selectedDataType = {
                     dataType: $scope.dataTypes[2]
                 };
             })
               .catch(function (response) {
                   alertService.add('error', 'Error while getting Data types  Error: ' + response.statusText);
               });

            //GET BUYERS
            repository.getbuyers()
             .then(function (response) {
                 $scope.buyers = response.data;

                 $scope.selectedBuyer = {
                     buyer: $scope.buyers[2]
                 };
                })
               .catch(function (response) {
                   alertService.add('error', 'Error while getting Buyers    Error: ' + response.statusText);
               });

            $scope.dataUploaderFieldHasError = function (dataUploaderForm) {
                if (!dataUploaderForm) {
                    return false;
                }
                return dataUploaderForm.$invalid && dataUploaderForm.$dirty;
            };

        }]);