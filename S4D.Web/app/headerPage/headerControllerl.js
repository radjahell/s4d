﻿'use strict';

angular.module('jarapp')
        .controller('HeaderController', ['$rootScope',  '$state', 'translateService', 'alertService', '$sessionStorage', function ($rootScope, $state, translateService, alertService, $sessionStorage) {

        var header = this;

        header.isUserLoggedIn = $sessionStorage.loggedIn;

        header.isVisible = function () {
                return $state.current.name !== "home"; 
            } 

        header.changeLanguage = function (langKey) {
            translateService.changeLanguage(langKey);
            alertService.add('success', 'Language changed to: ' + langKey);
        };

        header.logOut = function () {
            $sessionStorage.loggedIn = false;
            $state.go('home');
        };

    }]);