﻿angular.module('jarapp') 
 .service('repository', ['$http', 'appSettings', function ($http, appSettings) {

        this.getPriceList = function (id) {
             var priceList = $http.get(appSettings.serverPath + '/api/pricelist', { params: { "id": id } });
             return priceList;
          };

          this.getPriceLists = function () {  
              return $http.get(appSettings.serverPath + '/api/pricelist') ;
          };

          this.getSupplier = function (id) {
              var supplier = $http.get(appSettings.serverPath + '/api/supplier', { params: { "id": id } });
              return supplier;
          };

          this.getSuppliers = function () {
              return $http.get(appSettings.serverPath + '/api/supplier');
          };

          this.getpricelisttypes = function () {
              return $http.get(appSettings.serverPath + '/api/getpricelisttypes');
          };

          this.getdatatypes = function () {
              return $http.get(appSettings.serverPath + '/api/getdatatypes');
          };

          this.getbuyers = function () {
              return $http.get(appSettings.serverPath + '/api/getbuyers');
          };

          var config = {
              headers: {
                  'Content-Type': 'application/json'
                  //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
              }
          }; 
          this.sendFile = function (sentFileId) {
              
              var sellerId = 1;
              var buyerId = 1;
              var fileId = sentFileId;
              var postPath = appSettings.serverPath + '/api/saveFileInfo/' + sellerId + '/' + buyerId + '/' + fileId;

              return $http.post(postPath);


              //    $http({  
              //        url: appSettings.serverPath + '/api/savepriceList',  
              //        dataType: 'json',  
              //        method: 'POST',  
              //        data: priceListViewModel,  
              //        headers: {  
              //            "Content-Type": "application/json"  
              //        }  
              //    }).success(function (response) {  
              //        $scope.value = response;  
              //    })  
              //.error(function (error) {  
              //    alert(error);  
              //});  
              //}  

              //var priceListViewModel = { PersonId: 1, Name: "James" }

              //return $http({
              //    url: appSettings.serverPath + '/api/savepriceList',
              //    method: "POST",
              //    data: priceListViewModel,
              //    //contentType: "application/json",
              //    headers: {
              //        //'Content-Type': 'application/json'
              //        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
              //    }
              //});
         };

 }]);
 
 

