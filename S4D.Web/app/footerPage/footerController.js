﻿'use strict';

angular.module('jarapp')
        .controller('FooterController',  ['$state', function ($state) {

            var footer = this;

            footer.isVisible = function () {
                return $state.current.name  === "home"; 
            }

        }]);