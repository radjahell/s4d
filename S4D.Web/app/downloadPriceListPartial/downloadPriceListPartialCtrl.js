﻿

angular.module('jarapp')
        .controller('DownloadPriceListPartialController', ['$rootScope' , '$state', 'ngTableParams', '$filter', 'alertService', 'translateService', 'priceListService', function ($rootScope,  $state, ngTableParams, $filter, alertService, translateService, priceListService) {

        var downloadPriceListPartial = this;

        downloadPriceListPartial.priceListStatuses = [
             { id: "", title: "" }, { id: "Draft", title: "Draft" }, { id: "Approved", title: "Approved" }, { id: "Declined", title: "Declined" }, { id: "Update", title: "Update" }
            ];

        downloadPriceListPartial.selectRow = function (priceList) {
                priceListService.addOnePriceList(priceList);
                $state.go('home.dataPortDetails');

                //repository.getPriceList(priceList.Id).then(function (htmlData) {
                //    $scope.priceList = htmlData.data;
                //}).catch(function (response) {
                //     alertService.add('error', 'Error while retrieving Price Lists. Error: ' + response);
                //}); 
            };

        downloadPriceListPartial.tableParams = new ngTableParams({
                page: 1,
                count: 10
            }, {
                total: $rootScope.priceLists.length,
                getData: function ($defer, params) {
                    downloadPriceListPartial.data = params.sorting() ? $filter('orderBy')($rootScope.priceLists, params.orderBy()) : $rootScope.priceLists;
                    downloadPriceListPartial.data = params.filter() ? $filter('filter')(downloadPriceListPartial.data, params.filter()) : downloadPriceListPartial.data;
                    downloadPriceListPartial.data = downloadPriceListPartial.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    $defer.resolve(downloadPriceListPartial.data);
                }
            });
            //$scope.tableParams = new ngTableParams({
            //    page: 1,
            //    count: 1000

            //}, {
            //    counts: [],
            //    getData: function ($defer, params) {


            //        var htmlData = priceListService.getAllPriceLists();
            //        //var filteredData = params.filter() ?
            //        //              $filter('filter')(htmlData.data, params.filter()) :
            //        //              htmlData.data;

            //        //console.log(filteredData);
            //        //var orderedData = params.sorting() ?
            //        //    $filter('orderBy')(filteredData, params.orderBy()) :
            //        //    filteredData;
            //        //    params.total(orderedData.length);

            //        var resolve = htmlData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            //        $defer.resolve(htmlData.slice(resolve));



            //        //repository.getPriceLists().then(function (htmlData) { 

            //        //    var filteredData = params.filter() ?
            //        //              $filter('filter')(htmlData.data, params.filter()) :
            //        //              htmlData.data;

            //        //    var orderedData = params.sorting() ?
            //        //    $filter('orderBy')(filteredData, params.orderBy()) :
            //        //    filteredData;
            //        //    params.total(orderedData.length);

            //        //    var resolve = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            //        //    $defer.resolve(orderedData.slice(resolve)); 

            //        //    }).catch(function (response) {
            //        //        alertService.add('error', 'Error while retrieving Price Lists. Error: ' + response);
            //        //    }); 
            //    }, $scope: { $data: $scope.data }
            //});
            //$scope.tableParams.settings().$scope = $scope; //Fixes $emit error

        }]);