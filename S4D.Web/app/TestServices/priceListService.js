﻿'use strict';

angular
    .module('TestServices', [])
    .factory('priceListService', priceListService);

priceListService.$inject = ['$rootScope'];

function priceListService($rootScope, $localStorage) {

    $rootScope.priceList = {};

    $rootScope.priceLists = [{ "id": 0, "status": "Approved", "sender": "Philip@tesco.cz", "listName": "January Sales", "priceListType": "Pricelist", "fileName": "JanuarySalex.xlsx", "url": "29.107.35.8", "startDate": "12.04.2016", "endDate": "12.06.2016" },
                     { "id": 1, "status": "Declined", "sender": "Judith@nestle.cz", "listName": "February Goods", "priceListType": "Pricelist", "fileName": "MarchXX.xls", "url": "173.65.94.30", "startDate": "19.03.2016", "endDate": "12.04.2016" },
                     { "id": 2, "status": "Update", "sender": "Julie@tesco.cz", "listName": "March XX", "priceListType": "Pricelist", "fileName": "MarchXX.xlsx", "url": "9.100.80.145", "startDate": "13.08.2016", "endDate": "12.09.2016" },
                     { "id": 3, "status": "Approved", "sender": "Gloria@toto.cz", "listName": "Update June", "priceListType": "Pricelist", "fileName": "updateJune.xlsx", "url": "69.115.85.157", "startDate": "16.05.2016", "endDate": "12.06.2016" },
                     { "id": 4, "status": "Draft", "sender": "Gloriana@xoxo.cz", "listName": "Update July", "priceListType": "Pricelist", "fileName": "UpdateJuly.xlsx", "url": "69.115.85.157", "startDate": "12.06.2016", "endDate": "12.06.2016" },
                     { "id": 5, "status": "Declined", "sender": "James@toto.cz", "listName": "Update February", "priceListType": "Pricelist", "fileName": "UpdateFebruary.xlsx", "url": "69.115.85.157", "startDate": "13.09.2016", "endDate": "12.06.2016" },
                     { "id": 6, "status": "Declined", "sender": "Alex@xyxy.cz", "listName": "June Data", "priceListType": "Pricelist", "fileName": "JuneData.xlsx", "url": "69.115.85.157", "startDate": "18.07.2016", "endDate": "12.06.2016" },
                     { "id": 7, "status": "Draft", "sender": "Norbert@yopmail.cz", "listName": "November Fix", "priceListType": "Pricelist", "fileName": "updateJune.xlsx", "url": "69.115.85.157", "startDate": "17.03.2016", "endDate": "13.06.2016" },
                     { "id": 8, "status": "Draft", "sender": "Guilaume@toto.cz", "listName": "January June", "priceListType": "Pricelist", "fileName": "JanuaryJune.xlsx", "url": "69.115.85.157", "startDate": "15.04.2016", "endDate": "14.06.2016" },
                     { "id": 9, "status": "Update", "sender": "Curt@desingX.cz", "listName": "May June", "priceListType": "Pricelist", "fileName": "MayJune.xlsx", "url": "69.115.85.157", "startDate": "16.05.2016", "endDate": "16.06.2016" },
                     { "id": 10, "status": "Approved", "sender": "Dale@makro.cz", "listName": "December Tesco", "priceListType": "Pricelist", "fileName": "NewDecembertesco.xlsx", "url": "128.72.13.52", "startDate": "17.02.2016", "endDate": "17.03.2016" }];

    var getOnePriceList = function () {
        return $rootScope.priceList;
    };

    var setStatusToApprove = function(id) {
      //  $rootScope.priceLists[id].status = "Approved";
    };

    var addOnePriceList = function (priceList) {
        $rootScope.priceList = {
            id: priceList.id,
            sender: priceList.sender,
            status: priceList.status,
            listName: priceList.listName,
            priceListType: priceList.priceListType,
            fileName: priceList.fileName,
            size: priceList.size,
            url: priceList.url,
            file: priceList.file,
            startDate: priceList.startDate,
            endDate: priceList.endDate,
            gridOptions: priceList.gridOptions
        }; 
    };

    var addPriceList = function (priceList, gridOptions) {
        $rootScope.priceLists.push({
            id: priceList.id,
            sender: priceList.sender,
            status: priceList.status,
            listName: priceList.listName,
            priceListType: priceList.priceListType,
            fileName: priceList.fileName,
            size: priceList.size,
            url: priceList.url,
            file: priceList.file,
            startDate: priceList.startDate,
            endDate: priceList.endDate,
            gridOptions: gridOptions

        });
    };

    var getGridOptions = function () {
        return $rootScope.gridOptions;
    };

    var getAllPriceLists = function () {
        return $rootScope.priceLists;
    };

    var removePriceList = function (index) {
        $rootScope.priceLists.splice(index, 1);
    };

    var clearPriceList = function () {
        $rootScope.priceLists = [];
    };

    return {
        setStatusToApprove : setStatusToApprove,
        addPriceList: addPriceList,
        removePriceList: removePriceList,
        clearPriceList: clearPriceList,
        getAllPriceLists: getAllPriceLists,
        getGridOptions: getGridOptions,
        addOnePriceList: addOnePriceList,
        getOnePriceList: getOnePriceList
    };
}

