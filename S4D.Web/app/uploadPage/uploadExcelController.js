﻿'use strict';

angular.module('jarapp')
.controller('UploadExcelController', ['$scope', function ($scope) {
  
    $scope.gridOptions = {};
  
    $scope.reset = function () {
        $scope.gridOptions.data = [];
        $scope.gridOptions.columnDefs = [];
    }
}])