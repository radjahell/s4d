﻿

angular.module('jarapp')
        .controller('HomeController', ['$q', '$window', '$rootScope', '$scope', 'ngTableParams', '$filter', 'repository', 'alertService', '$state', 'priceListService', '$sessionStorage', 'typesService', function ($q, $window, $rootScope, $scope, ngTableParams, $filter, repository, alertService, $state, priceListService, $sessionStorage, typesService) {
            $scope.gridOptions = {};

            $scope.showPreview = true;
            $scope.loggedInUser = $sessionStorage.loggedInUserName;
            $sessionStorage.loggedInUserName = "info@solution4data.com";
            $scope.loggedInUserName = '';
            $scope.loggedIn = $sessionStorage.loggedIn; 

            $scope.excelFile = '';

            ////FLOW
            //$scope.uploader = {}; 
            //$scope.uploadFlow = function () {
            //    $scope.uploader.flow.upload();
            //};
            ////FLOW

            $scope.goToDownloads = function () { 
                $state.go('home.dataPort');
            };

            $scope.priceListTypes = typesService.getDataTypes();

            $scope.selectedPriceListType = {
                priceListType: $scope.priceListTypes[2]
            };

            $scope.dataUploadReceivers = typesService.getDataUploadReceivers();

            $scope.selectedDataUploadReceiver = {
                dataUploadReceiver: $scope.dataUploadReceivers[3]
            };

            $scope.loginFieldHasError = function (loginForm) {
                if (!loginForm) {
                    return false;
                }
                return loginForm.$invalid && loginForm.$dirty;
            };

            $scope.saveLogin = function (loginForm, login) {
                if (login.inputEmail === $sessionStorage.loggedInUserName) {
                    $scope.loggedInUser = $sessionStorage.loggedInUserName;
                    $sessionStorage.loggedIn = true;
                } else {
                    $sessionStorage.loggedIn = false;
                } 
                $rootScope.$emit('NotifyPartialUploadDataPartialViewEvent');
                $scope.loggedIn = $sessionStorage.loggedIn;
                $scope.loggedInUserName = login.inputEmail;
            }

        }]);