﻿'use strict';

angular
    .module('Alerts', [])
    .factory('alertService', alertService);

alertService.$inject = ['$rootScope'];

function alertService($rootScope) {

    // create an array of alerts available globally
    $rootScope.alerts = [];

    var addAlert = function (type, msg, alertData) {
        /// <summary>
        /// Adds an alert.
        /// </summary>
        /// <param name="type">Alert type: 'success', 'info', 'error'</param>
        /// <param name="msg">Alert message.</param>
        /// <param name="alertData">Alert additionnal data.</param>
        $rootScope.alerts.push({ 'type': type, 'msg': msg, data: alertData });
    };

    var removeAlert = function (index) {
        $rootScope.alerts.splice(index, 1);
    };

    var clearAlert = function () {
        $rootScope.alerts = [];
    };

    return {
        add: addAlert,
        remove: removeAlert,
        clear: clearAlert
    };
}


//<div class="col-sm-offset-3 col-sm-6">
//           <div class="panel panel-default" ng-show="alerts.length > 0">
//               <div class="panel-heading">
//                   <h3 class="panel-title"><a id="alertsPanel"></a>Alerts: </h3>
//               </div>
//               <div class="panel-body">
//                   <div ng-repeat="alert in alerts" class="alert alert-dismissable" ng-class="{'alert-success': alert.type == 'success', 'alert-info': alert.type == 'info', 'alert-danger': alert.type == 'error'}">
//                       <button type="button" class="close" aria-hidden="true" ng-click="closeAlert($index)">&times;</button>
//                       <p><strong>Message:</strong>&nbsp;{{alert.msg}}</p>
//                       <div ng-show="alert.data">
//                           <a ng-click="alert.showData = !alert.showData" class="alert-link">Show/Hide alert data</a>
//                           <pre ng-show="alert.showData">{{alert.data}}</pre>
//                       </div>
//                   </div>
//               </div>
//           </div>
//       </div>