﻿angular.module('jarapp').directive(
    'sendFilePartialDirective',
    function () {
        return ({
            controller: "SendFilePartialController",
            controllerAs: "sendFile",
            link: link,
            restrict: "A",
            templateUrl: 'app/sendFilePartial/sendFilePartial.html'
        });

        function link(scope, element, attributes) {
        }

    }
);

