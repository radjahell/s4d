﻿

angular.module('jarapp')
        .controller('SendFilePartialController', ['priceListService', 'typesService', 'filesManagerRepository', 'alertService', 'repository', function (priceListService, typesService, filesManagerRepository, alertService, repository) {
  
            var sendFile = this; 

            sendFile.receivers = typesService.getReceivers();
            sendFile.postForm;
            sendFile.dataPost;


            sendFile.selectedReceiver = {
                receiver: sendFile.receivers[3]
            }; 

            //UPDATE FILE INFO
            sendFile.fileSuccess = function (file, message) {  

                var sentFile = { id: message, fileName: file.name, message: "MY TEST", size: "100" };


                repository.sendFile(message).then(function (response) {
                    alertService.add('success', 'Info Updated: ' + response.statusText);
                }).catch(function (response) {
                    alertService.add('error', 'Error while Sending File. Error: ' + response.statusText);
                });

            };
         

            //GET BUYERS
            repository.getbuyers()
             .then(function (response) {
                 sendFile.buyers = response.data;

                 sendFile.selectedBuyer = {
                     buyer: sendFile.buyers[2]
                 };
             })
               .catch(function (response) {
                   alertService.add('error', 'Error while getting Buyers    Error: ' + response.statusText);
               });



            sendFile.dataPostFieldHasError = function (dataPostForm) {
                if (!dataPostForm) {
                    return false;
                }
                return dataPostForm.$invalid && dataPostForm.$dirty;
            };

            
            sendFile.uploader = {};
            sendFile.uploadProgress = 0;
      

            sendFile.saveDataPost = function (dataPostForm, dataPost) { 
                sendFile.uploader.flow.upload(); 
                sendFile.postForm = dataPostForm;
                sendFile.dataPost = dataPost; 
            }
        }]);