﻿angular.module('jarapp')
   .directive("fileread", [function () {
       return {
           scope: {
               opts: '='
           },
           link: function ($scope, $elm, $attrs) {
               $elm.on('change', function (changeEvent) {
                   var reader = new FileReader();

                   reader.onload = function (evt) {
                       $scope.$apply(function () {
                           var data = evt.target.result;

                           var workbook = XLSX.read(data, { type: 'binary' });

                           var headerNames = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];

                           var data = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);

                           $scope.opts.columnDefs = [];
                           headerNames.forEach(function (h) {
                               $scope.opts.columnDefs.push({ field: h });
                           });

                           $scope.opts.data = data;

                           $elm.val(null);
                       });
                   };

                   reader.readAsBinaryString(changeEvent.target.files[0]);
               });
           }
       }
   }]).directive('ngFileModel', ['$parse', function ($parse) {
       return {
           restrict: 'A',
           link: function (scope, element, attrs) {
               var model = $parse(attrs.ngFileModel);
               var isMultiple = attrs.multiple;
               var modelSetter = model.assign;
               element.bind('change', function () {
                   var values = [];
                   angular.forEach(element[0].files, function (item) {
                       var value = {
                           // File Name 
                           name: item.name,
                           //File Size 
                           size: item.size,
                           //File URL to view 
                           url: URL.createObjectURL(item),
                           // File Input Value 
                           _file: item
                       };
                       values.push(value);
                   });
                   scope.$apply(function () {
                       if (isMultiple) {
                           modelSetter(scope, values);
                       } else {
                           modelSetter(scope, values[0]);
                       }
                   });
               });
           }
       };
   }]);