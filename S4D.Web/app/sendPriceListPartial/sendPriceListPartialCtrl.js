﻿

angular.module('jarapp')
        .controller('SendPriceListPartialController', ['$scope', 'priceListService', 'typesService', function ($scope, priceListService,  typesService) {
             

            $scope.saveDataUploader = function (dataUploaderForm, dataUploader) { 
                 
            }; 

            $scope.priceListTypes = typesService.getDataTypes();

            $scope.selectedPriceListType = {
                priceListType: $scope.priceListTypes[2]
            };

            $scope.dataUploadReceivers = typesService.getDataUploadReceivers();

            $scope.selectedDataUploadReceiver = {
                dataUploadReceiver: $scope.dataUploadReceivers[3]
            }; 

            $scope.dataUploaderFieldHasError = function (dataUploaderForm) {
                if (!dataUploaderForm) {
                    return false;
                }
                return dataUploaderForm.$invalid && dataUploaderForm.$dirty;
            };
 
        }]);