

//PUT METHODS AngularJS + C#

var editUrlCustomisationsApi = function (currentApplicationID, urlCustomisations) {
            var deferred = $q.defer();
 
            var url = apiUrl + '/UrlCustomisations/UrlCustomisationsUpdate/' + currentApplicationID;
            //var params = { params: { "applicationID": currentApplicationID, "urlCustomisations": urlCustomisations } };
 
            $http.put(url, urlCustomisations)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    deferred.reject(response);
                });
 
            return deferred.promise;
        };
 
 
 
[HttpPut]
        [ActionName("UrlCustomisationsUpdate")]
        public HttpResponseMessage Put(ApplicationId applicationID, [FromBody]UrlCustomisations urlCustomisations)
        {
            try
            {
                _urlCustomisationsBuilder.UpdateUrlCustomisations(applicationID, urlCustomisations);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                if (_logger != null)
                {
                    _logger.Error("Error updating url customisations.", ex);
                }
                throw;
            }
        }
 
 
            context.Routes.MapHttpRoute(
            name: "CRMUrlCustomisationsUpdate",
            routeTemplate: "crm/api/UrlCustomisations/UrlCustomisationsUpdate/{applicationID}",
            defaults: new { controller = "UrlCustomisations", action = "UrlCustomisationsUpdate" });


//PUT METHODS AngularJS + C#

//DataDownloader -
//http://stackoverflow.com/questions/21680768/export-to-xls-using-angularjs
//http://jsfiddle.net/TheSharpieOne/XNVj3/1/

//https://github.com/nervgh/angular-file-upload
//https://github.com/danialfarid/ng-file-upload
 
//http://stackoverflow.com/questions/31404502/upload-multiple-files-in-angular

//Download
//http://jsfiddle.net/onury/5gWFL/

//HTML FILE INPUT ACEEPT
http://stackoverflow.com/questions/11832930/html-input-file-accept-attribute-file-type-csv

//import spreadsheet
//http://brianhann.com/easily-import-spreadsheets-into-ui-grid/

//http://www.jasny.net/bootstrap/getting-started/#download
//MODAL
//http://www.dwmkerr.com/the-only-angularjs-modal-service-youll-ever-need/
//ngtable
//http://4dev.tech/2015/08/tutorial-basic-datatable-sorting-filtering-and-pagination-with-angularjs-and-ng-table/

//CACHING IN $HTTP
//https://www.themarketingtechnologist.co/caching-http-requests-in-angularjs/